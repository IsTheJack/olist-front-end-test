/* eslint import/no-extraneous-dependencies: ["error", {"peerDependencies": true}] */
import React from 'react';

import { storiesOf, action } from '@storybook/react'; // eslint-disable-line

// Components
import Button from '../components/Button';
import Logo from '../components/Logo';
import SignupForm from '../components/SignupForm';
import StrengthIndicator from '../components/StrengthIndicator';
import TextInput from '../components/TextInput';

// CSS
import '../index.css';

storiesOf('Button', module)
  .add('Button Component', () => (
    <Button
      text="Enviar"
      onClick={action('clicked')}
    />
  ));

storiesOf('Logo', module)
  .add('Logo Component', () => <Logo />);

storiesOf('SignupForm', module)
  .add('SignupForm Component', () => <SignupForm onSubmit={() => undefined} />);

storiesOf('StrengthIndicator', module)
  .add('StrengthIndicator Component', () => (
    <StrengthIndicator
      strengthItems={[
        ['Pelo menos 6 caracteres', true],
        ['Pelo menos 1 letra maiúscula', false],
        ['Pelo menos 1 número', undefined],
      ]}
    />
  ));

storiesOf('TextInput', module)
  .add('TextInput Component', () => (
    <TextInput
      label="Nome completo"
      type="text"
      placeholder="Digite seu nome"
    />
  ))
  .add('Custom border color', () => (
    <TextInput
      label="Nome completo"
      type="text"
      borderColor="#00E7AA"
      placeholder="Digite seu nome"
    />
  ));
