import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import SignupForm from './components/SignupForm';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<SignupForm onSubmit={() => null} />, document.getElementById('root'));
registerServiceWorker();
