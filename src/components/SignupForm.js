import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import Button from './Button';
import Logo from './Logo';
import StrengthIndicator from './StrengthIndicator';
import TextInput from './TextInput';

// Utils
import passwordValidation, { getIndividualValidations } from '../utils/passwordValidation';

class SignupForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      password: '',
      confirmation: '',
    };
  }

  get passwordColorValidation() {
    if (!this.state.password) return undefined;

    const isValid = passwordValidation(this.state.password)
      .filter(validation => !!validation).length >= 3;

    if (isValid) {
      return '#00E7AA';
    }

    return '#FF9084';
  }

  get confirmationColorValidation() {
    const {
      password,
      confirmation,
    } = this.state;

    if (!confirmation) return undefined;
    if (confirmation === password) {
      return '#00E7AA';
    }

    return '#FF9084';
  }

  preventSubmit(e, onSubmit) {
    e.preventDefault();
    onSubmit(e);
  }

  render() {
    return (
      <section className="signup-form">

        <div className="signup-form__header">
          <Logo />
          <h1 className="signup-form__title">Crie sua conta</h1>
        </div>

        <form
          onSubmit={e => this.preventSubmit(e, this.props.onSubmit)}
          method="post"
        >
          <TextInput
            label="Nome completo"
            name="name"
            required
          />

          <TextInput
            label="E-mail"
            name="email"
            type="email"
            required
          />

          <TextInput
            label="Senha"
            name="password"
            type="password"
            required
            borderColor={this.passwordColorValidation}
            onChange={e => this.setState({ password: e.target.value })}
          />

          <StrengthIndicator
            strengthItems={getIndividualValidations(this.state.password)}
          />

          <TextInput
            label="Confirme sua senha"
            name="confirmation"
            type="password"
            required
            borderColor={this.confirmationColorValidation}
            onChange={e => this.setState({ confirmation: e.target.value })}
          />

          <Button
            type="submit"
            text="Criar Conta"
          />
        </form>
      </section>
    );
  }
}

SignupForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default SignupForm;
