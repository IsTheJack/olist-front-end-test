import React from 'react';

// Assets
import logoImage from '../assets/olist.svg';

const Logo = () => (
  <div className="logo">
    <img
      className="logo__image"
      src={logoImage}
      alt="Olist"
    />
  </div>
);

export default Logo;
