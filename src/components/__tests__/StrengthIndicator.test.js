import React from 'react';
import ReactDOM from 'react-dom';
import StrengthIndicator from '../StrengthIndicator';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    (
      <StrengthIndicator
        strengthItems={[
          ['Pelo menos 6 caracteres', undefined],
          ['Pelo menos 1 letra maiúscula', undefined],
          ['Pelo menos 1 número', undefined],
        ]}
      />
    ), div,
  );
});
