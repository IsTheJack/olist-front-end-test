import React from 'react';
import PropTypes from 'prop-types';

const StrengthIndicator = ({
  strengthItems,
}) => (
  <div className="strength-indicator">
    <div className="strength-indicator__step-container">
      {strengthItems.map(item => <div key={item[0]} className={`strength-indicator__step--${item[1]}`} />)}
    </div>

    <ul className="strength-indicator__item-text-container">
      {strengthItems.map(item => (
        <li
          key={item[0]}
          className={`strength-indicator__item-text--${item[1]}`}
        >
          {item[0]}
        </li>
      ))}
    </ul>
  </div>
);

StrengthIndicator.propTypes = {
  strengthItems: PropTypes.arrayOf(PropTypes.array).isRequired,
};

export default StrengthIndicator;
