import React from 'react';
import PropTypes from 'prop-types';
import { omit } from 'ramda';

const TextInput = props => (
  <div className="text-input">
    <p className="text-input__label">
      {props.label}
    </p>

    <input
      className="text-input__raw-input"
      style={{ borderColor: props.borderColor }}
      {...omit(['borderColor', 'label'], props)}
    />
  </div>
);

TextInput.propTypes = {
  label: PropTypes.string.isRequired,
  borderColor: PropTypes.string,
};

TextInput.defaultProps = {
  borderColor: '#B2B8CD',
};

export default TextInput;
