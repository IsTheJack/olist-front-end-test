import React from 'react';
import PropTypes from 'prop-types';

const Button = ({
  text,
  onClick,
  type,
}) => (
  <button
    className="button"
    onClick={onClick}
    type={type}
  >
    {text}
  </button>
);

Button.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  type: PropTypes.string,
};

Button.defaultProps = {
  type: '',
  onClick: () => undefined,
};

export default Button;
