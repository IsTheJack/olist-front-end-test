import {
  __,
  length,
  gte,
  pipe,
  test,
  map,
  reduce,
  equals,
  any,
} from 'ramda';

const validationKeys = [
  'minLenght',
  'expectOneOrMoreUpperChar',
  'expectOneOrMoreNumberChar',
];

export const validationsFunction = {
  minLenght: pipe(length, gte(__, 6)),
  expectOneOrMoreUpperChar: test(/[A-Z]/),
  expectOneOrMoreNumberChar: test(/[0-9]/),
};

export const validationsMessageError = {
  minLenght: 'Pelo menos 6 caracteres',
  expectOneOrMoreUpperChar: 'Pelo menos 1 letra maiúscula',
  expectOneOrMoreNumberChar: 'Pelo menos 1 número',
};

export const getIndividualValidations = (password) => {
  const validations = passwordValidation(password);
  return validationKeys
    .map((validationKey, i) => [validationsMessageError[validationKey], validations[i]]);
};

export default function passwordValidation(password) {
  if (!password) return [undefined, undefined, undefined];

  const getValidations = map(validationKey => validationsFunction[validationKey](password));
  const fillWithUndefinedAfterError = reduce((acc, validation) => {
    const hasAnyFalse = any(equals(false))(acc);

    if (hasAnyFalse) return [...acc, undefined];
    return [...acc, validation];
  }, []);

  const validations = pipe(getValidations, fillWithUndefinedAfterError)(validationKeys);

  return validations;
}
