import passwordValidation, { getIndividualValidations } from '../passwordValidation';

describe('util: passwordValidation', () => {
  it('Must return 3 undefineds if is empty string', () => {
    const validPassword = '';
    const validations = [undefined, undefined, undefined];
    expect(passwordValidation(validPassword)).toEqual(validations);
  });

  it('Must pass if is validated', () => {
    const validPassword = 'aSdfg7';
    const validations = [true, true, true];
    expect(passwordValidation(validPassword)).toEqual(validations);
  });

  it("Mustn't pass if char size isn't validated", () => {
    const invalidPassword = 'aSdf7';
    const validations = [false, undefined, undefined];
    expect(passwordValidation(invalidPassword)).toEqual(validations);
  });

  it("Mustn't pass if upper char isn't validated", () => {
    const invalidPassword = 'asdfg7';
    const validations = [true, false, undefined];
    expect(passwordValidation(invalidPassword)).toEqual(validations);
  });

  it("Mustn't pass if number char isn't validated", () => {
    const invalidPassword = 'asdfGh';
    const validations = [true, true, false];
    expect(passwordValidation(invalidPassword)).toEqual(validations);
  });
});

describe('util: getIndividualValidations', () => {
  it('Must pass if is validated', () => {
    const mockValidationResult = [
      ['Pelo menos 6 caracteres', undefined],
      ['Pelo menos 1 letra maiúscula', undefined],
      ['Pelo menos 1 número', undefined],
    ];

    const emptyPass = '';
    expect(getIndividualValidations(emptyPass)).toEqual(mockValidationResult);
  });
});

