# Olist Front-end Test

## Candidado: Roberto Oliveira da Silva
* Idade: 24
* Cidade: Resende (RJ)
* [Github](https://github.com/IsTheJack)
* [LinkedIn](https://www.linkedin.com/in/robertooliveirasilva/)
* [Perfil no Medium (Artigos Feitos)](https://medium.com/@roliveiradev)
  * [[O que é?] Programação (Cláudio e o Teorema de Pitágoras)](https://medium.com/roliveiradev/o-que-%C3%A9-programa%C3%A7%C3%A3o-cl%C3%A1udio-e-o-teorema-de-pit%C3%A1goras-e397da7e662b)
  * [Por que Usamos Immutable.js](https://blog.getty.io/por-que-usamos-immutable-js-6ef85a4d4604)
  * [Debugando React Native No Android Via Wi-Fi](https://blog.getty.io/debugando-react-native-no-android-via-wi-fi-cedce242c375)
* [AppTrivium (Projeto onde atuo como idealizador e empreendedor)](https://apptrivium.com)

O Projeto foi feito usando React, SASS (BEM) e Jest, sendo configurados através do CRA (Create React App).

Os componentes podem ser visualizados através do StoryBook que foi configurado no projeto.

```
npm run storybook
```

![Imagem do Projeto](./readme_files/story.png)

### Qualidade do código

#### Testes

Os componentes estão com testes simples pra verificar se estão quebrando ao serem usados. (Taxa de cobertura de 63%)

Existe um arquivo util para validação de senha que foi iniciado usando TDD. (Taxa de cobertura de 100%)

![Cobertura de Testes](./readme_files/coverage.png)

#### Linter

Foi configurado o ESLint do Airbnb.

#### Git Hooks

Foi configurado o Husky para rodar hooks de Git de testes e lint a cada commit e a cada push.

#### CI

Configurado o CircleCI pra verificar testes e lint nas branches; master, dev e acc.

O Mesmo poderia ser utilizado pra automatizar deploys nas branches; master (produção) e acc (aceitação/homologação).

![Circle CI](./readme_files/ci.png)

#### CSS (Padrão BEM)

Em projetos modularizados e componentizados, eu uso o padrão BEM criando escopos e protegendo o resto do projeto para que toda regra escrita de um componente não interfira em outro.

#### Padrão de Pastas

![Padrão de Pastas](./readme_files/folder.png)

* assets
  * Arquivos do projeto como imagens, fontes, vídeos e outros.
* components
  * Onde vai todos os componentes do projeto. Jogando todos os componentes na mesma pasta, se torna garantido que não haverá componentes com o mesmo nome de arquivo. Cada nome de arquivo é o nome do próprio componente e o nome do seu bloco em css (em padrão snake case).
* stories
  * Onde vai toda configuração de componentes do Storybook.
* utils
  * Onde são criados os arquivos de helpers internos do projeto.
